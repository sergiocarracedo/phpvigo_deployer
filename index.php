<?php

require 'vendor/autoload.php';
require 'settings.php';

$smarty = new Smarty();
$smarty->setCacheDir($settings['tpl_cache_dir']);
$smarty->setTemplateDir($settings['tpl_dir']);
$smarty->setCompileDir($settings['tpl_compile_dir']);


$imgs = glob($settings['user_img_dir'] . '/*.jpg');

$smarty->assign(array(
  'base_path' => $settings['base_path'],
  'imgs' => $imgs,
));

$smarty->display('index.tpl');