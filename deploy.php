<?php

// All Deployer recipes are based on `recipe/common.php`.
require 'recipe/common.php';
require './.deployer/myrecipe.php';

task('deploy', [
  'deploy:prepare',
  'deploy:release',
  'deploy:update_code',
  'deploy:shared',
  'deploy:writable',
  'deploy:symlink',
  'cleanup'
]);

serverList('.deployer/servers.yml');

set('keep_releases', 5);

set('repository', 'git@bitbucket.org:sergiocarracedo/phpvigo_deployer.git');
