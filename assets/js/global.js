$(function() {
   $('.matchHeight').matchHeight({
       byRow: true,
       property: 'height',
       target: null,
       remove: false
   });
});